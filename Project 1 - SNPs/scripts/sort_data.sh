# # This script will order the data within each bwaDn folder. 
# # This script should only be performed on the GIT copy of 
# # the project, as the functional scripts require that the bwaDn 
# # folders are *not* further subdivided! 

# This script is designed to be executed from within the reads dir 
# so it does not require any additional arguments. Alternatively, 
# it is possible to provide the path to the reads folder as argument $1

# This script will sort the bwaDn datafiles into: 
# --> bwaDn 
#     --> QC_Reports
#         --> html
#             - html documents from fastqc 
#         --> json
#             - json documents from fastqc 
#         --> zip
#             - zip files from fastqc
#     --> bam 
#         - *.bam 
#         - *.bam.bai 
#     --> ref 
#         - reference genome files 
#     --> fastq 
#         --> raw
#             - Dn_1/2.fastq.gz
#         --> processed
#             - *trim.fastq.gz
#
# It will also sort the files within the snp folder. As it will also remove reference genome etc. in the process, 
# it will make a copy of the folder in the process. 
# It requires that reads and snp are in the same directory!
#
# --> snp 
#     --> D{n}
#         - all vcf files of D{n}


USR=student02
USR_DIR=/data/users/$USR
SERVER_READS_DIR=$USR_DIR/reads

MUTANTS="5 6 7 8"

# check you are not trying to alter the server directory!
if [[ -d $SERVER_READS_DIR ]]; then 
    echo "You are on the server!"
    echo "This script can only be performed on your local machine"
    echo "as it is required that the original project directory is not altered!"
else 
    
    # get reads directory
    if [[ $PWD == *"reads" ]]; then 
        READS_DIR=$PWD
    else 
        READS_DIR=$1
    fi

    # now start sorting the data
    cd $READS_DIR
    for mutant in $(ls); do 

        cd $READS_DIR/$mutant
        mkdir -p QC_Reports/html QC_Reports/json QC_Reports/zip 
        mv *html QC_Reports/html
        mv *json QC_Reports/json
        mv *zip QC_Reports/zip

        mkdir bam 
        mv *.bam bam 
        mv *.bam.bai bam
        
        mkdir ref 
        mv R64* ref

        mkdir -p fastq/raw fastq/processed
        mv *trim.fastq.gz fastq/processed
        mv *.fastq.gz fastq/raw

        echo "Sorting of $mutant finished"

    done

    # now we continue with the data in snp 

    cd $READS_DIR
    cd .. 
    SNP_SORT_DIR=snp_sorted
    mkdir $SNP_SORT_DIR
    cd snp
    for i in $MUTANTS; do 
        mkdir $SNP_SORT_DIR/$i
        cp D$i*vcf $SNP_SORT_DIR/$i
    done
    
    # rename snp_orig to snp_orig
    cd ..
    mv snp snp_orig

fi