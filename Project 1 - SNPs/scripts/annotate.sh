# This is a wrapper for the single_annotate.slurm script 
# It will call on the single_annotate.slurm script for each .vcf file within the snp subfolder



USR=student02
USR_DIR=/data/users/$USR
READS_DIR=$USR_DIR/reads
SCRIPT_DIR=$USR_DIR/scripts/

cd $USR_DIR/snp


for i in $(ls *bam.vcf); do 
    sbatch $SCRIPT_DIR/single_annotate.slurm $i
done