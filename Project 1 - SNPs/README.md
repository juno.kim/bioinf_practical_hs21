# BioInf_Practical_HS21

### The Practical Repo for the Bioinformatics Practical 2021 
> Course: SBC.07107 Bioinformatics (practicals + in silico) [SA 21]

---

This repository documents the the workflow, data, and results of the first project of the Bioinformatics Practical: **Identification of Single-Nucleotide-Polymorphisms in Yeast (Saccharomyces cerevisiae)**.)

## Data Availability 
---
> #### Disclaimer: 
> Due to the filesize it is currently not possible to directly attach the data folder to this repo 
> We are working to fix this issue. 
> However, the analysis is performed on a `csv` copy of the entire dataset's `vcf` results which can be found in the Analysis subfolder

### reads folder
Data can be found in the `data` folder, which includes a separate subfolder for each analysed yeast mutant strain $D_n$ (foldername `bwaD{n}`). 
Each folder has a number of subdirectories to simplify file browsing. It should be noted that the original project folders *do not* sort files in `bwaD{n}` into subfolders but contain all files unstructured. 

#### fastq subfolder
Each `bwaD{n}` folder contains `fastq` sequencing reads as `D{n}_{i}.fastq.gz` where `i = (1, 2)` for `fwd` and `rev`, respectively (**raw subfolder**). 
Alongside these initial data files, intermediary processing results are also included. `*trim.fastq.gz` files were generated using `fastp` (**processed subfolder**). 

#### bam subfolder
Genome mapping was done with `samtools` and the resulting `bam` file can be found as `D{n}.bam`. 

#### ref subfolder
The reference yeast genome `R64-1-1.104.fa` with index `R64-1-1.104.fa.fai` is also included.

#### QC_Reports subfolder
Quality control reports generated from `fastqc` are in `json` and `html` format (also as zipped archive). 

### snp folder
The SNP annotations can be found in the `snp` folder. 
This folder, too, was subdivided into $D_n$ subfolders, which the original project directory did not. Also, links of the reference genome and `.bam` files were removed from this copy of the directory. 


## Methods in Brief
---
A brief overview of the workflow will be given here. Scripts used to perform the described steps are available in the `scripts` folder in this repository. 

### Initial Setup
Raw data was taken from the server directory and linked into a the local `reads` folder. In the following code fragments `$XX` denotes the mutant identifier $D_n$. 

 Processing steps with `fastqc` for quality control and `fastp` for cleanup were done using

```bash
#verify that your files are in your directory (ls -l)
fastqc -t 2 ${XX}_1.fastq.gz ${XX}_2.fastq.gz
```

Cleanup with `fastp` was done as

```bash
fastp -i ${XX}_1.fastq.gz -I ${XX}_2.fastq.gz -o ${XX}_1trim.fastq.gz -O ${XX}_2trim.fastq.gz -j ${XX}_fastp.json -h ${XX}_fastp.html --thread 4 --trim_poly_g -l 150;
```

This was followd by genome mapping of the sequencing reads to the reference genome was done with `samtools` using 

```bash 
samtools view -b -@ 4 -t R64-1-1.104.fa.fai ${XX}.sam > ${XX}_unsorted.bam
samtools sort -@ 4 -o ${XX}.bam ${XX}_unsorted.bam
samtools index ${XX}.bam
```

All these steps were wrapped in the script `mapdoitall.slurm`, which was provided by the course supervisor. 


## SNP Identification
Identification of SNPs (single nucleotide polymorphisms) was done using `bcftools` as 

```bash 
bcftools mpileup -Ou --threads 8 -f R64-1-1.104.fa ${XX} | bcftools call -vc -Oz --threads 8 -o ${XX}.vcf.gz
# indexing
tabix ${XX}.vcf.gz
```

These steps were encoded in the script `single_snpcall.slurm`. 

## SNP Classification
Annotation of SNPs was done using `SnpSift`. Therewhile, results were filtered to only contain matches in coding regions and hits that were not common to all mutants analysed. 

Classification steps were encoded in the script `single_annotate.slurm`. 

## Data Analysis in Brief 
---
Data analysis code can be found in the `Analaysis` directory. Analysis was done using the sorted data structure within this repository, as opposed to the original structure on the server. 

