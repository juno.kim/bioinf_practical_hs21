%\begin{multicols}{3}
\section{Methods}


%Notes: 
%- Basically the strategy to identify possible gene candidates is as follows: 
%If we loose Acl4, the chaperone responsible for importing RPL4, then we are bound to loose RPL4 from the Ribosome, making the ribosome probably non-functional (or at least less functional, thus explaining the lethal phenotype). However, if some replacement for Acl4 could be found that was able to import RPL4 into the nucleolus for ribosomal assembly instead of the lost Acl4 we would be able to revert the phenotype. This is one possibility. On the other hand, it might be that a ribosome component mutated to be able to replace the lost RPL4, making the ribosome functional even though RPL4 is no longer present. This is a second possibility. A third possibility might be that an entirely new gene emerged (maybe from duplication or whatever) that was able to replace the lost RPL4. Such a protein would have to be very similar to RPL4, however. 
%
%Strategies for identifying possible candidates for these three scenarios are now: 
%(1) Identify proteins that could be interacting with RPL4 – according to sequence-based PPI predictions. The chief challange here is that this is a targeted approach that does not specify any prior criteria for gene selection. It would stand to reason that more highly mutated genes would be better candidates in this category. Furthermore, a manual selection based on known protein properties (such as a general function as a nuclear importer) would be in order. 
%    One criterion that would come to mind is that a protein that would replace Acl4 would probably be similar in sequence to Acl4, so BLAST searching for similar proteins based on the Acl4 sequence might provide a set of candidates, which could then be used to query the dataset.
%    EDIT: We performed BLASTp against ACl4 but only found some hypothetical proteins. While this in itself should not be considered of major significance, it could indicate that some as yet unknown gene locus (perhaps a product of some duplication of Acl4 that used to be inactive did mutate to become active once more and thus rescue the phenotype). In fact, the very fact that loss of Acl4 produced not a lethal but only slow-growth phenotype suggests that there should be at least one other factor able to take on it's functional duties in chaperoning RPL4. It is likely that this factor would have mutated in the suppressor mutants to produce a rescue phenotype. 
%    However, if it were the case that some perviously unknown locus is responsible for the phenotype, then our dataset would not reveal said locus as we restrict ourselves to known coding-regions.
%    The fact that loss of RPL4 is lethal to cells (at least double loss of A and B paralogs) strongly suggests that there must be at least one other factor capable of RPL4 nuclear import, given that it is highly unlikely that cells would simply cope with the loss of RPL4 (unless scenario 2 were to be true, see below...)
%(2) If RPL4 is to be replaced by some other component of the ribosome, it would stand to reason that said replacement would be located at a similar position within the large subunit as RPL4. For that reason, it would also stand to reason to assume that it is actually a neighbour of RPL4 or at least has been classified as an "interactor" of RPL4 based on some form of biochemical assay such as co-IP. In that case, a given set of known interactors of RPL4 could be used to query the dataset for mutated genes that are part of this set. 
%
%(3) If an entirely new protein emerged that replaced RPL4, then two problems would arise. Firstly, the new protein would have to be able to import into the nucleolus by some means, and secondly, it would have to be able to fit into the biological setting of RPL4. The latter suggests that said hypothetical protein would show great similarity to the original RPL4, meaning that the problem of the lost Acl4 would not actually disappear, as a highly similar protein to RPL4 would most likely also rely on the same import machinery. Hence, this last option can probably be excluded. 


\subsection{Previous Work}
\citeauthor{our_main_paper} prepared genomic DNA libraries for multiple suppressor mutant clones and performed whole-genome sequencing by Illumina\trademark, as described in their paper \cite{our_main_paper}. Resulting raw datasets were provided for four such clones D5, D6, D7, and D8 in FastQ format for subsequent analysis (this work). 

\subsection{Procedure in Brief and Availability}
In this project, we mapped sequence reads onto the yeast genome, identified variant genetic loci and assessed possible mutant gene candidates that may help explain the observed suppressor-phenotype in response to ACL4-depletion. Scripts were written for each of the steps outlined in-detail below and both source code and subsequent analysis results are available at \url{https://gitlab.com/NoahHenrikKleinschmidt/bioinf_practical_hs21}. 

\subsection{Read Mapping}
To assess quality of the sequencing data, \texttt{fastqc} software was applied to raw \texttt{fastq} data files. Reads were sanitized using \texttt{fastp}, and quality was re-checked using \texttt{fastqc}.
As reference genome yeast genome R64 (R64-1-1.104) was used. Reads were mapped using \texttt{samtools}. 

\subsection{SNP Identification and Classification}
SNPs were identified using \texttt{bcftools}, and subsequently indexed using \texttt{tabix}. To further annotate / classify the found SNPs the software \texttt{SnpSift} was used. 

\subsection{Data Analysis}
The resulting annotated datasets from \texttt{SnpSift} were loaded as \texttt{pandas DataFrames} and analysed in \texttt{Jupyter} and visualised using \texttt{IGV}. Promising gene candidates were further vetted with help of NCBI BLAST \cite{NCBI}, the SGD Database \cite{SGD}, Uniprot \cite{uniprot}, and PantherDB \cite{panther}.% , and PSOPIA Webserver. % PSOPIA would be nice to cite but for this the server has to work and not get stuck all the time.......

\subsubsection{Search Strategy}
In order to identify possible mutant gene candidates the following strategy was followed. Considering the importance of both RPL4 and ACL4, as well as their biological context (see Introduction) the following would stand to reason. In case of ACL4-loss RPL4 would also loose its efficient import and assembly pipeline and hence cells would assemble RPL4-lacking ribosomes. In order to suppress this effect, three possible cases emerge. (case 1) Some other molecular chaperone takes over the role of ACL4 and mediates alternative RPL4 nuclear import and ribosomal assembly. In this case ACL4-loss would be directly compensated. (case 2) Alternatively, some other ribosomal constituent protein takes over the role of the lost RPL4, thus compensating for RPL4-loss instead. In this scenario RPL4 remains lost to the ribosome but is (at least partly) replaced by a neighbouring constituent. Related to this case, it may be conceivable that an aberrant ribosome biogenesis factor may have guided alternative incorporation of an otherwise non-mutant ribosomal protein, thereby creating an alternative but stabilised (and presumably functional) large ribosomal subunit (case 3), or that alternative pre-rRNA processing took place that resulted in stabilisation of the large subunit despite loss of RPL4 (case 4).

\\

As all of these scenarios mark major changes in protein behaviour it would be most likely that some high-impact mutation (e.g. a stop-loss, or large indel) should be present within a given candidate gene. Also, it is probable, that more than one single mutation is responsible for the given phenotypic change, making single-mutant genes less promising candidates. Furthermore, considering that all mutant clones display the same phenotype it would also stand to reason that they would contain the same underlying mutant gene or set of genes, hence genes that are mutated in all strains are likely better candidates than genes that are only mutated in one or two strains. 
\\

All of these possible cases would entail a different set or preliminaries that allow a separate search strategy each, which are outlined below. 
\\

In order to search for possible replacement candidates for ACL4 (case 1), two approaches were taken. Firstly, the reference genome was searched for sequence-similar proteins to ACL4 as these would have a higher likelihood to easily replace ACL4 (Approach 1). 
Secondly, a reference list of genes known to be involved in nuclear import, molecular chaperones, and ribosome biogenesis, was generated based on Gene Ontology terms (Approach 2). Both of these approaches generated some set of possible gene candidates which were queried against the dataset in order to identify promising candidates. 
\\

In order to assess possible candidates to have replaced RPL4 (case 2), a set of all large ribosomal subunit proteins (from Gene Ontology) was used and queried against the dataset (Approach 3). 
\\

% now integrated in the paragraph above...
%Yet another conceivable case might be that an aberrant rRNA processing factor generates an aberrant but presumably functional 25S rRNA that is not dependent on RPL4 anymore (case 4). 
%\\

As the so far outlined approaches were targeted in nature, we further browsed the dataset for candidates in an unbiased strategy. To that end we selected genes based on their mutation rate (number of mutations found per gene) (Approach 4). A second approach also we followed was to select genes based on their mutation impact (classified by \texttt{SnpSift}), where we focused on genes containing mutations classified as "high impact" (e.g. stop-gain) (Approach 5). Both of these unbiased approaches yielded a set of possible candidates, which were reverse-annotated using PantherDB.
\\

Finally, manual vetting of candidate genes was done by consulting known literature and database entries, as well as bioinformatic tools for \textit{in silico} prediction of protein-protein interaction such as the PSOPIA Webserver \cite{PSOPIA}. 


%\end{multicols}