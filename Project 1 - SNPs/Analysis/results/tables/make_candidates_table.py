"""
This script will generate a summary 
table of all interesting candidates 
specified in primary_candidates.txt
"""

import pandas as pd 
import re 

# read datasets
candidates = pd.read_table("../../primary_candidates.txt")
main_prots = pd.read_csv("../../main_df_with_prots.csv")


def get_prot_info(string):
    ID = re.search("(\(.+\))", string).group(1)
    tmp = string.replace(ID, "")
    tmp = tmp.lower()
    tmp = list(tmp)
    tmp[0] = tmp[0].upper()
    tmp = "".join(tmp)
    tmp = tmp + ID
    return tmp

def get_strains(candidate):
    tmp = main_prots.query(f"gene == '{candidate}'")
    strains = list(set(tmp["name"]))
    if len(strains) == 4: 
        strains = "all"
    else: 
        strains = ", ".join(strains)
    return strains 

def count_mutations(candidate, unique = False):
    tmp = main_prots.query(f"gene == '{candidate}'")
    if unique:
        phens = tmp["p_phen"]
        counts = [list(phens).count(i) for i in set(list(phens))]
        counts = len(counts)
    else:
        counts = tmp.groupby(["p_phen", "name"]).count()
        counts = sum(counts["prot"])
    return counts

def get_impacts(candidate):
    tmp = main_prots.query(f"gene == '{candidate}'")
    impacts = list(set(tmp["impact"]))
    if len(impacts) > 1:
        impacts = " / ".join(impacts)
    else: 
        impacts = impacts[0]
    for i, j in zip(["LOW", "MODERATE", "HIGH"], ["*", "***", "*****"]):
        impacts = impacts.replace(i,j)
    return impacts

def get_position(candidate):
    tmp = main_prots.query(f"gene == '{candidate}'")
    chrom = list(set(tmp["CHROM"]))[0]
    pos = min(set(tmp["POS"]))
    position = f"{pos} ({chrom})"
    print(position)
    return position

# assemble a raw-text csv table 
all_entries = ["Gene,Protein,Description,Position,Identified By,Total Mutations,Impact"]
for candidate, prot, description, identified_by, comment in zip(candidates["gene"], candidates["prot"], candidates["prot_info"], candidates["identified_by"], candidates["comment"]):
    if "excluded" in str(comment):
        continue
    description = get_prot_info(description)
    # strains = get_strains(candidate)
    position = get_position(candidate)
    mutations = count_mutations(candidate)
    impacts = get_impacts(candidate)
    assembly_entry = f"{candidate},{prot},{description},{position},{str(identified_by)},{mutations},{impacts}"
    all_entries.append(assembly_entry)

all_entries = "\n".join(all_entries)

with open("candidates.csv", "w") as of:
    of.write(all_entries)

