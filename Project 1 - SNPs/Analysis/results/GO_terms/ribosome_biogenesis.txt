
YEAST|SGD=S000002241|UniProtKB=P0CX52	YDL083C	40S ribosomal protein S16-B;RPS16B;ortholog	40S RIBOSOMAL PROTEIN S16 (PTHR21569:SF16)	ribosomal protein(PC00202)	Saccharomyces cerevisiae
YEAST|SGD=S000004751|UniProtKB=P0CX51	YDL083C	40S ribosomal protein S16-A;RPS16A;ortholog	40S RIBOSOMAL PROTEIN S16 (PTHR21569:SF16)	ribosomal protein(PC00202)	Saccharomyces cerevisiae
YEAST|SGD=S000005731|UniProtKB=Q08622	YOR205C	Genetic interactor of prohibitins 3, mitochondrial;GEP3;ortholog	GENETIC INTERACTOR OF PROHIBITINS 3, MITOCHONDRIAL (PTHR46434:SF1)		Saccharomyces cerevisiae
YEAST|SGD=S000005336|UniProtKB=P53742	YNR053C	Nucleolar GTP-binding protein 2;NOG2;ortholog	NUCLEOLAR GTP-BINDING PROTEIN 2 (PTHR11089:SF9)		Saccharomyces cerevisiae
