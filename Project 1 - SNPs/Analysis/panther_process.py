"""
This module is designed to help with processing the results files exported from the PantherDB

There are essentially two applications for this module

The user can either load a number of subset-files from pantherDB output 
using the function "read_subset_files" and/or a total results list using "read_index"
As the index file will contain both the pantherDB identifier and the original ID used for mapping
it is highly recommended to always use an index file. Subset files and index files can be combined using 
"index_data". 
"""
import pandas as pd
import os
import re 

INFO_STATS = {
    "gene" : 1, 
}

DROP_COLS = ["id2", "organism", "info", "prot2", "_"]

def _read(filename):
    cols = ["id", "id2", "info", "prot", "prot2", "organism"]
    contents = pd.read_table(filename, sep = "\t", names = cols)
    _update_infos(contents)
    return contents

def _update_infos(df):
    # applies _extrac_from_info(row) and updates the df
    infos = {}
    for row in df["info"]: 
        tmp = _extract_from_info(row)
        for t in tmp: 
            if t in infos:
                infos[t].append(tmp[t])
            else: 
                infos[t] = [tmp[t]]
    for info in infos:
        df[info] = infos[info]



def _extract_from_info(row):
    """ 
    This function extracts information from INFO column
    It will extract all entries specified inthe INFO_STATS
    """    
    tmp = row.split(";")
    results = {}
    for stat, idx in zip(list(INFO_STATS.keys()), list(INFO_STATS.values())): 
        
        results.update(
            {stat : tmp[idx]}
        )

    return results

def drop_info(df):
    # drops all columns specified by DROP_COLS constant list
    new_df = None
    for i in DROP_COLS: 
        try: 
            new_df = df.drop([i], axis=1)
        except: 
            pass
    return new_df 

def read_subset_files(dir):
    """
    Reads all the files within a given directory and generates an overall df
    """
    total_df = []
    cwd = os.getcwd()
    os.chdir(dir)
    files = os.listdir()
    files = [i for i in files if (not i.startswith(".") and not i.startswith("_"))]
    for file in files: 
        tmp = _read(file)
        tmp["src"] = [_subset_id(file) for i in tmp["id"]]
        total_df.append(tmp)
    total_df = pd.concat(total_df, ignore_index=True)
    total_df = _crop_uniqes_and_count(total_df)
    os.chdir(cwd)
    return total_df

def _subset_id(filename):
    # gets the in-brackets subset name
    subset_id = r".+\((.+)\).+"
    subset_id = re.search(subset_id, filename)
    subset_id = subset_id.group(1)
    return str(subset_id)

def _crop_uniqes_and_count(total_df):
    """
    Eliminates redundant gene entries and counts how often each gene occurred.
    """
    counts = total_df.groupby(["gene", "src"]).count().reset_index()
    counts = list(counts["id"])
    total_df =  total_df.groupby(["gene", "src"]).last().reset_index()
    total_df["subsets"] = counts
    return total_df


def read_index(filename):
    """
    Reads the index file from pantherDB (all mapped IDs alongside their identifiers)
    """
    cols = ["id", "mapped_id", "info", "prot", "_", "organism"]
    contents = pd.read_table(filename, sep="\t", names = cols)
    _update_infos(contents)
    #contents = contents[["id", "mapped_id"]]
    #contents = _crop_uniqes_and_count(contents)
    return contents


def index_data(df, index):
    new_df = index.merge(df, right_on = "id", left_on = "id")
    return new_df

