# BioInf_Practical_HS21

### The Practical Repo for the Bioinformatics Practical 2021 
> Course: SBC.07107 Bioinformatics (practicals + in silico) [SA 21]

---

This repository documents the the workflow, data, and results of the practical course in Bioinformatics. 
The two separate projects conducted during this practical are located in respective subfolders. 

## Data Availability 
---
> #### Disclaimer: 
> Due to the filesize it is currently not possible to directly attach the data folder to this repo 
> We are working to fix this issue. 

