# BioInf_Practical_HS21

### The Practical Repo for the Bioinformatics Practical 2021 
> Course: SBC.07107 Bioinformatics (practicals + in silico) [SA 21]

---

This repository documents the the workflow, data, and results of the second project of the Bioinformatics Practical: **De Novo Genome Assembly of Tsetse Fly**.



## Data
---

### asm_tse2 folder
Contains link to raw reads `tse2.fastq` cleaned reads `tse2_clean.fastq` as well as assembly files `asm_hifiasm.*`.

#### Compare_asm
Contains assembly files to be compared using QUAST.

#### QC folder
Quality control reports generated from `fastqc` are in `html` format. 

#### QUAST folder
Contains the results of the QUAST assembly comparision, whcih includes a `html` file.

## Methods in Brief
---
A brief overview of the workflow will be given here. Scripts used to perform the described steps are available in the `scripts` folder in this repository. 

### Raw data and cleaning
Raw data `tse2.fastq` was taken from the server directory and linked into a the local `asm_tse2` folder. 

The raw data reads were cleaned using fastp and assembled using hifiasm. The corresponding script `asmdoitall.slurm` is located in the scripts folder. The resulting assembly files `asm_hifiasm.p_ctg_gfa` and `asm_hifiasm.p_ctg.fa` can be found in the `asm_tse2` and the `Compare_asm` folder.

Raw data and cleaned data underwent quality checking using fastqc. The results are located in the `QC` folder in `html` format.

### Purging haplotigs
The previously generated assembly files `asm_hifiasm.p_ctg_gfa` and `asm_hifiasm.p_ctg.fa` were purged from haplotigs using `purge_dups`. The corrsponsing script `runpurgedup.slurm` is located in the `scripts` folder. The results `hifiasm.p_ctg.purged.fa` and `hifiasm.p_ctg.hap.fa` are located in the `Compare_asm` folder.

### Comparing assemblies with QUAST
The assemblies were compared using QUAST. The corresponding script `runQUAST.slurm` is in the scripts folder. The script output is located in the `QUAST` folder and inlcudes `report.html`.

### Comparing assemblies with Bandage
The `*.gfa` graph files can be downloaded onto a local machine and compared by opening them with Bandage (https://rrwick.github.io/Bandage/).

### BUSCO and DOT
no idea...
